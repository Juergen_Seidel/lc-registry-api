﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCRegistryAPI
{
    public class Project
    {
        public string ProjectSerial { get; set; }
        public int LastEmission { get; set; }

    }
}
