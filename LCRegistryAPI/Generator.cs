﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace LCRegistryAPI
{
    public class Generator
    {
        static readonly char[] validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();
        static readonly RandomNumberGenerator crypto = RandomNumberGenerator.Create();

        public static string New()
        {
            char[] serial = new char[6];
            for (int i = 0; i < serial.Length; i++) serial[i] = validChars[RandomInt(0, validChars.Length)];
            return new string(serial);
        }

        static int RandomInt(int min, int max)
        {
            byte[] four_bytes = new byte[4];
            crypto.GetBytes(four_bytes);
            UInt32 scale = BitConverter.ToUInt32(four_bytes, 0);
            return (int)(min + (max - min) * (scale / (uint.MaxValue + 1.0)));
        }
    }
}
