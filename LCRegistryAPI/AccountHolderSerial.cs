﻿using Repository.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCRegistryAPI
{
    public class AccountHolderSerial : Entity
    {
        public string Serial {get; set;}
        public List<Project> Projects { get; set; } = new List<Project>();

    }
}
