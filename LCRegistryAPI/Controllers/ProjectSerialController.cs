﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Mongo;

namespace LCRegistryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectSerialController : ControllerBase
    {
        // GET: api/SerialNumber
        public ProjectSerialController(MongoDBService db)
        {
            this.db = db;
        }


        private readonly MongoDBService db;
        [HttpGet]
        [Authorize]
        public async Task<string> GetAsync(string AccountHolder)
        {
            AccountHolderSerial ah = db.AccountHolderRepo.Find(i => i.Serial == AccountHolder).FirstOrDefault();
            if (ah == null) return "Invalid account Holder";
            string serial = Generator.New();
            while (ah.Projects.Exists(i => i.ProjectSerial == serial)) serial = Generator.New();
            ah.Projects.Add(new Project() { ProjectSerial = serial });
            bool x = await db.AccountHolderRepo.ReplaceAsync(ah);
            return string.Format("{0}-{1}", AccountHolder, serial);
        }
    }
}
