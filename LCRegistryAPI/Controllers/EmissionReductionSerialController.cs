﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Mongo;

namespace LCRegistryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmissionReductionSerialController : ControllerBase
    {
        // GET: api/SerialNumber
        public EmissionReductionSerialController(MongoDBService db)
        {
            this.db = db;
        }

        private readonly MongoDBService db;

         [HttpGet]
        [Authorize]
        public async Task<string> GetAsync(string Project, int Range)
        {
            if (Range > 1000) return "Invalid range";
            if (Range == 0) Range = 1;
            string[] parts = Project.Split("-");
            if (parts.Count() != 2) return "Wrong format";
            AccountHolderSerial ah =  db.AccountHolderRepo.Find(i => i.Serial == parts[0]).FirstOrDefault();
            if (ah == null) return "Account holder unknown";
            Project p = ah.Projects.Find(i => i.ProjectSerial == parts[1]);
            if (p == null) return "Project unknown";
            string[] result = new string[Range];
            for (int i = 0; i < Range; i++)
            {
                result[i] = string.Format("{0}-{1:000000}", Project, p.LastEmission + i + 1); 
            }
            p.LastEmission += Range;
            await db.AccountHolderRepo.ReplaceAsync(ah);
            return string.Join(',', result);
        }
    }
}
