﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LCRegistryAPI.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        [Authorize]
        public string Get()
        {

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("023a1cba0270d72af9ba21a2768e9acbefbbf42e1e871f13c50f63932dcd0cba"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new SecurityTokenDescriptor() {
                Issuer = "GHG Accounting Services Ltd.",
                Audience = "Local Carbon Registry",
                //IssuedAt = DateTime.Now,
                //Expires = DateTime.Now.AddMinutes(30),
                SigningCredentials = creds,
                };

            return new JwtSecurityTokenHandler().CreateEncodedJwt(token);
        }
    }
}

// bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1MzA1MzAyMDMsImV4cCI6MTUzMDUzMzgwMywiaWF0IjoxNTMwNTMwMjAzLCJpc3MiOiJHSEcgQWNjb3VudGluZyBTZXJ2aWNlcyBMdGQuIiwiYXVkIjoiTG9jYWwgQ2FyYm9uIFJlZ2lzdHJ5In0.0EhbGXBadoAQrNCG2FuRrV-NdXGk1ESLVQVMqbifoiQ
