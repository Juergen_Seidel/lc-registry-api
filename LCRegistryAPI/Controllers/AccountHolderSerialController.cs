﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Mongo;

namespace LCRegistryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountHolderSerialController : ControllerBase
    {
        // GET: api/SerialNumber

        public AccountHolderSerialController(MongoDBService db) {
            this.db = db;
        }

        private readonly MongoDBService db;

        [HttpGet]
        [Authorize]
        public string Get()
        {
            string serial = Generator.New();
            while (db.AccountHolderRepo.Any(i=>i.Serial == serial)) serial = Generator.New();

            AccountHolderSerial ah = new AccountHolderSerial() { Serial = serial };
            db.AccountHolderRepo.Insert(ah);
            return new string(ah.Serial);
        }


    }
}
