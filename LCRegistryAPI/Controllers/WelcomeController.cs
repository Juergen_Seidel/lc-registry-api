﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LCRegistryAPI.Controllers
{
    [Route("api/")]
    public class WelcomeController : ControllerBase
    {
        // GET: api/<controller>
        [HttpGet]
        [AllowAnonymous]
        public string Get()
        {
            return "Welcome to Local Carbon Registy API";
        }
    }
}
