﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Repository.Mongo;
using Microsoft.Extensions.Configuration;

namespace LCRegistryAPI
{
    public class MongoDBService
    {
        public readonly Repository<AccountHolderSerial> AccountHolderRepo;

        public MongoDBService(IConfigurationSection configurationSection)
        {
            AccountHolderRepo = new Repository<AccountHolderSerial>(configurationSection["ConnectionDB"], "Serials");
        }
    }
}
